# Logo with Paint Brush Effect

Creating a 3D animation in Blender of a paint brush creating your logo, done following two great tutorials from [Blender Made Easy](https://www.youtube.com/channel/UCaS7BQniaSNfBZDYiLimKxQ).

## Frames

| First Frame                                 | Last Frame                                  | 
| :-----------------------------------------: | :-----------------------------------------: | 
| ![The last frame of my animation](0001.jpg) | ![The last frame of my animation](0220.jpg) | 


#### Sources

Tutorial 1: [Paint Brush in 3D with Blender](https://www.youtube.com/watch?v=xrw7-cbtQBA)

Tutorial 2: [Adding your Logo](https://www.youtube.com/watch?v=waoF3XpkPlE)
